// Exercise 1

var ex1HanderlerBtn = document.getElementById("ex1-handler-btn");

function ex1Handler(){
    console.log("ok");
    var ex1Input1 = document.querySelector("#ex1-input #ex-input-1").value*1;
    var ex1Input2 = document.querySelector("#ex1-input #ex-input-2").value*1;
    var resultEx1 = ex1Input1*ex1Input2;
    document.querySelector('#ex1-result #ex-result-text').innerHTML = resultEx1;
}

// Exercise 2

var ex2HanderlerBtn = document.getElementById("ex2-handler-btn");

function ex2Handler(){
    console.log("ex2 ok");
    var ex2Input1 = document.querySelector("#ex2-input #ex-input-1").value*1;
    var ex2Input2 = document.querySelector("#ex2-input #ex-input-2").value*1;
    var ex2Input3 = document.querySelector("#ex2-input #ex-input-3").value*1;
    var ex2Input4 = document.querySelector("#ex2-input #ex-input-4").value*1;
    var ex2Input5 = document.querySelector("#ex2-input #ex-input-5").value*1;
    var resultEx2 = (ex2Input1 + ex2Input2 + ex2Input3 + ex2Input4 + ex2Input5)/5;
    document.querySelector('#ex2-result #ex-result-text').innerHTML = resultEx2;
}

// Exercise 3

var ex3HanderlerBtn = document.getElementById("ex3-handler-btn");

function ex3Handler(){
    console.log("ex3 ok"); 
    var ex3Input1 = document.querySelector("#ex3-input #ex-input-1").value*1;
    var resultEx3 = ex3Input1*23500;
    document.querySelector('#ex3-result #ex-result-text').innerHTML = new Intl.NumberFormat('vn-VN').format(resultEx3) + " VND";
}

// Exercise 4

var ex4HanderlerBtn = document.getElementById("ex4-handler-btn");

function ex4Handler(){
    console.log("ex4 ok"); 
    var ex4Input1 = document.querySelector("#ex4-input #ex-input-1").value*1;
    var ex4Input2 = document.querySelector("#ex4-input #ex-input-2").value*1;
    var result1Ex4 = ex4Input1*ex4Input2;
    var result2Ex4 = (ex4Input1+ex4Input2)*2;
    document.querySelector('#ex4-result #ex-result-text').innerHTML = "Diện tích: " + result1Ex4;
    document.querySelector('#ex4-result #ex-result-text-2').innerHTML = "Chu vi: " + result2Ex4;
}

// Exercise 5

var ex5HanderlerBtn = document.getElementById("ex5-handler-btn");

function ex5Handler(){
    console.log("ex5 ok"); 
    var ex5Input1 = document.querySelector("#ex5-input #ex-input-1").value*1;
    if(ex5Input1 >= 10 && ex5Input1 <= 99){
        var hangDonVi = ex5Input1%10;
        var hangChuc = Math.floor(ex5Input1/10);
        var resultEx5 = hangDonVi + hangChuc;
        document.querySelector('#ex5-result #ex-result-text').innerHTML = resultEx5;
    } else {
        document.querySelector('#ex5-result #ex-result-text').innerHTML = "! Nhập số có 2 chữ số";
    }
}